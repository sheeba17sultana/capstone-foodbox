package com.food;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class FoodboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodboxApplication.class, args);
	
	}

}
